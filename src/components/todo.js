import React from 'react'
import { useState } from 'react';
import { useEffect } from 'react';
import Todoform from './todoform';
import "./todo.modules.css"
import Todolist from './todolist';

function Todo() {
    const [todoList, setTodoList] = useState([]);
    const [key, setKey] = useState(0);

    const addTodo = todo => {
        if (todo == null) {
            alert("Please enter");
        }
        const temp = { key: key, content: todo };
        setTodoList([...todoList, temp])
        var count = key + 1;
        setKey(count);
    }

    useEffect(() => {
        console.log(todoList);
    }, [todoList])

    const edit = (id,value) => {
        if (value == null) {
            alert("Please enter");
        }
        let lis = todoList.map((item) => {
            if (item.key == id) {
                console.log(id);
                return { ...item, content: value };
            }
            return item;
        })

        setTodoList(lis);

    }

    const del = id => {
        setTodoList(todoList.filter(todo => todo.key !== id))
    }
    useEffect(() => {
        var record = JSON.parse(localStorage.getItem("record"));
        if (record != null || record.length != 0) {
            // localStorage.clear();
            setKey(record.length-1)
            setTodoList(record);
            // console.log({ todoList );
        }
    }, []);


    useEffect(() => {
        localStorage.setItem("record", JSON.stringify(todoList));
    }, [todoList])

    return (
        <div className='wrap'>
            <div className='divmain'>
                <h1>TODO List</h1>
                <Todoform onAdd={addTodo} />
                <div className='listwrap'>
                <Todolist todoList={todoList} deleteItem={del} editItem={edit} />
                </div>
            </div>
        </div>
    )
}

export default Todo
