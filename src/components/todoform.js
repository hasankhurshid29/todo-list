import React, { useEffect } from 'react'
import { useState } from 'react';
import { AiOutlinePlusSquare } from 'react-icons/ai';
import { AiOutlineEdit } from 'react-icons/ai';



function Todoform(props) {
    const [value, setValue] = useState(props.edit ? props.edit.content : '');
    const handleChange = (event) => {
        setValue(event.target.value);
    }

    const handleEdit = () => {
        
        console.log(props.edit);
        props.onClick(value);
        setValue("");
    }

    const handleAdd = () => {
        console.log(props.edit);
        props.onAdd(value);
        setValue("");
    }

    
  return (
    <div>
        
        {props.edit ? <div className='button-input-wrap'>
            <input className='AddInput' type="text" value={value} onChange={handleChange} />
        <button className='buttonForm' onClick={handleEdit} type="submit" value="Enter"><AiOutlineEdit   className='edit-icon up'/></button>
        </div> : <div className='button-input-wrap'><input className='AddInput' type="text" value={value} onChange={handleChange} />
        <button className='buttonForm' onClick={handleAdd} type="submit" value="Enter"><AiOutlinePlusSquare className='plus-icon' /></button></div>}
    </div>
  )
}

export default Todoform
