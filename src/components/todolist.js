import React, { useState } from 'react';
import "./todo.modules.css";
import { AiOutlineEdit } from 'react-icons/ai';
import { AiOutlineDelete } from 'react-icons/ai';
import Todoform from './todoform';

function Todolist({ todoList, deleteItem, editItem }) {

    const [edit, setEdit] = useState({
        key: null,
        content: ''
    });

    const submitUpdate = value => {
        editItem(edit.key, value);
        setEdit({
          key: null,
          content: ''
        });
      };


    if (edit.key) {
        console.log(edit);
        return <Todoform edit={edit} onClick={submitUpdate} />;
        
    }


    return todoList.map(list =>
        <div className="item">
            <p>{list.content}</p>
            <div className="todoBtn">
                <button onClick={() => deleteItem(list.key)}><AiOutlineDelete className='delete-icon'/></button>
                <button onClick={() => setEdit({ key: list.key, content: list.content })}><AiOutlineEdit className='edit-icon'/></button>
            </div>
        </div>
    )

}

export default Todolist
