import React from "react";
import { useState } from "react";

class Form extends React.Component {

    constructor(props) {
        super(props);
        this.state = { value: '' };
        this.todo = [];


        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        event.preventDefault();
        this.todo.push(this.state.value);
        console.log(this.todo);
    }


    render() {
        return (
            <div style={{ margin: 'auto' }}>
                <form onSubmit={this.handleSubmit}>
                    <label>
                        Item:
                        <input type="text" value={this.state.value} onChange={this.handleChange} />
                    </label>
                    <input type="submit" value="Enter" />
                {this.todo.map(list => <p>{list}</p>)}
                </form>
                <br />
            </div>
        );
    }
}

export default Form