import React, { useEffect } from "react";
import { useState } from "react";
import styles from "./todo.module.css";
import deleteIcon from "./images/delete.svg";


function Todo() {
    const [todoList, setTodoList] = useState([]);
    const [value, setValue] = useState("")
    const [key, setKey] = useState(1)
    const [edit, setEdit] = useState("")
    const [editKey, setEditKey] = useState()

    const [editBtn, seteditBtn] = useState(false);
    const [isUpdate, setIsUpdate] = useState(false);

    console.log("todoList", todoList);




    const handleChange = (event) => {
        setValue(event.target.value);
    }

    const deleteItem = (k) => {
        setTodoList(todoList.filter(todo => todo.key !== k))
    }

    const editItem = (key) => {
        // seteditBtn(true);
        // console.log(todoList);
        // setValue(todoList[key - 1].content);
        // setEditKey(key)
    }

    const handleEdit = (event) => {
        if (value == null) {
            alert("Please enter");
        }
        let lis = todoList.map((item) => {
            if (item.key == editKey) {
                return { ...item, content: value };
            }
            return item;
        })

        setTodoList(lis);
        seteditBtn(false);
        setValue("")

    }

    const handleSubmit = (event) => {
        if (value === "") {
            alert("Please enter");
        }
        console.log("value", value)
        const temp = { key: key, content: value };
        setTodoList([...todoList, temp]);
        // setIsUpdate(true);
        var count = key + 1;

        setKey(count)
        setValue('')
    };

    useEffect(() => {
        var record = JSON.parse(localStorage.getItem("record"));
        if (record != null || record.length != 0) {
            console.log("loclastorage :", record);
            // localStorage.clear();
            setTodoList(record);
            // console.log({ todoList );
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("record", JSON.stringify(todoList));
    }, [todoList])


    return (
        <div className={styles.wrapper}>
            <div className={styles.maindiv}>
                <h1>TODO LIST</h1>
                <div className={styles.inputText}>
                    <input type="text" value={value} onChange={handleChange} />

                    <div>
                        {
                            editBtn ?
                                <button onClick={handleEdit} type="submit" value="Enter">Edit</button> :
                                <button onClick={handleSubmit} type="submit" value="Enter">Add</button>
                        }

                    </div>
                </div>
                {todoList.map(list =>
                    <div className={styles.item}>
                        <p>{list.key + ": "} {list.content}</p>
                        <div className={styles.todoBtn}>
                            <button onClick={() => deleteItem(list.key)}><img src={deleteIcon} /></button>
                            <button onClick={() => editItem(list.key)}><i class="fa-thin fa-pen"></i></button>
                        </div>
                    </div>
                )}


            </div>
        </div>
    );


}

export default Todo
